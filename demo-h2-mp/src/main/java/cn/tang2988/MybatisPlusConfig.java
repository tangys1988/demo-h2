package cn.tang2988;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.optimize.JsqlParserCountOptimize;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Configuration
@MapperScan("cn.tang2988")
public class MybatisPlusConfig {
	/**
	 * 分页插件
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		// 开启 count 的 join 优化,只针对 left join !!!
		PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
		paginationInterceptor.setDbType(DbType.ORACLE);
//		paginationInterceptor.setDbType(DbType.MYSQL);
		return paginationInterceptor.setCountSqlParser(new JsqlParserCountOptimize(true));
	}
}
