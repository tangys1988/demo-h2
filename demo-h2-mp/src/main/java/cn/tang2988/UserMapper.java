package cn.tang2988;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  如果只使用内置BaseMapper的CURD方法，可不需要 UserMapper.xml
 * </p>
 *
 * @author hubin
 * @since 2018-08-11
 */
public interface UserMapper extends BaseMapper<User> {

}
