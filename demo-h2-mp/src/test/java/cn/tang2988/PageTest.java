package cn.tang2988;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.tang2988.User;
import cn.tang2988.UserMapper;

/**
 * <p>
 * 内置 CRUD 演示
 * </p>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PageTest {
	@Resource
	private UserMapper mapper;

	@Test
	public void selectPage() {
		Page<User> page = mapper.selectPage(new Page<>(2, 3), Wrappers.<User>query().orderByAsc("age"));
		assertThat(page).isNotNull();
		assertThat(page.getRecords()).isNotEmpty();
		System.out.println(JSON.toJSONString(page, true));
	}

	@Test
	public void selectMapsPage() {
		IPage<Map<String, Object>> page = mapper.selectMapsPage(new Page<>(2, 3),
				Wrappers.<User>query().select("id", "name").orderByAsc("age"));
		assertThat(page).isNotNull();
		assertThat(page.getRecords()).isNotEmpty();
		assertThat(page.getRecords().get(0)).isNotEmpty();
		System.out.println(JSON.toJSONString(page, true));
	}
}
