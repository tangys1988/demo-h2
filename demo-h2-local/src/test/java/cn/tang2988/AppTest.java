package cn.tang2988;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.h2.tools.Server;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AppTest {

	@Test
	public void nativeDurableConn() throws SQLException {
		// 本地数据库会在指定目录生产数据文件。本例会在家目录生产testDB_nativeDurableConn.mv.db
		Connection conn = DriverManager.getConnection("jdbc:h2:~/testDB_nativeDurableConn", "sa1", "sa");// 用户名，密码随意设置
		log.info("{}", conn.getMetaData());
		conn.close();
	}

	@Test
	public void nativeMemConn() throws SQLException {
		// 内存数据库不会生产数据文件，存在内存中。随jvm退出清除
		Connection conn = DriverManager.getConnection("jdbc:h2:mem:testDB_nativeMemConn", "sa1", "sa");// 用户名，密码随意设置
		log.info("{}", conn.getMetaData());
		conn.close();
	}

	@Test
	public void tcpConn() throws SQLException {// 只有tcp模式才可以被多个客户端连接，本地和内存模式都只支持单个jvm内部访问数据库
		{
			// 本地数据库会在指定目录生产数据文件。本例会在家目录生产testDB_tcpServer.mv.db
			Connection conn = DriverManager.getConnection("jdbc:h2:~/testDB_tcpServer", "sa1", "sa");// 用户名，密码随意设置
			log.info("nativeConn:{}", conn.getMetaData());
			conn.close();

			// 因为客户端连接需要指定数据库名，所以提供TCP服务时，必须先初始化一个数据库，再启动tcp服务
			String[] serverParams = new String[] { "-tcp", "-tcpPort", "9123", "-tcpAllowOthers", "-webAllowOthers",
					"-pgAllowOthers" };
			Server server = Server.createTcpServer(serverParams).start();
		}

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				// 客户端连接
				Connection conn;
				try {
					conn = DriverManager.getConnection("jdbc:h2:tcp://localhost:9123/~/testDB_tcpServer", "sa1", "sa");
					log.info("tcpConn:{}", conn.getMetaData());
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
		t.start();

		while (t.isAlive()) {
		}
		System.out.println("over");
	}

}
